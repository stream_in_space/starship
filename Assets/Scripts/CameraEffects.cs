﻿using UnityEngine;

namespace Starship
{

    public class CameraEffects : MonoBehaviour
    {
        // Start is called before the first frame update
        private SmoothFollow smoothFollow;
        [SerializeField] private float smooth = 5;
        private float startFollowingDistance;
        private float targetDistance;
        private float targetHeight;
        private float startTargetHeight;
        private float boostedDistance;
        private float boostetHeight;
        void Start()
        {
            smoothFollow = Camera.main.GetComponent<SmoothFollow>();
            startFollowingDistance = smoothFollow.distance;
            startTargetHeight = smoothFollow.height;
            boostetHeight = startTargetHeight / 2;
            boostedDistance = startFollowingDistance / 2;
        }
        public void OnSpeedBoost()
        {
            targetDistance = boostedDistance;
            targetHeight = boostetHeight;
            ApplySpeedBoostingEffect();
        }
        public void OnNormalSpeed()
        {
            targetDistance = startFollowingDistance;
            targetHeight = startTargetHeight;
            ApplySpeedBoostingEffect();
        }
        private void ApplySpeedBoostingEffect()
        {
            smoothFollow.distance = Mathf.Lerp(smoothFollow.distance, targetDistance, smooth * Time.deltaTime);
            smoothFollow.height = Mathf.Lerp(smoothFollow.height, targetHeight, smooth * Time.deltaTime);
        }
    }
}

