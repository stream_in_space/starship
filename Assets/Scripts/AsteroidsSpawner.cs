﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Starship
{

    public class AsteroidsSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject asteroidPrefab;
        [SerializeField] private int spawnDistance = 150;
        [SerializeField] private float spawnRate = 2;
        [HideInInspector] public List<GameObject> availableAsteroids;
        private GameManager gameManager;

        //The pool of asteroids. If the pool is empty - create a new one, if objects are in the pool - reuse them 
        private GameObject GetAsteroid()
        {
            if (availableAsteroids.Count > 0)
            {
                GameObject oldAsteroid = availableAsteroids[0];
                availableAsteroids.RemoveAt(0);
                oldAsteroid.SetActive(true);
                return oldAsteroid;
            }
            else
            {
                GameObject newAsteroid = GameObject.Instantiate(asteroidPrefab);
                newAsteroid.transform.SetParent(gameManager.raodSystem.road.transform);
                return newAsteroid;
            }
        }
        private void Start()
        {
            gameManager = GameManager.Instance;
            availableAsteroids = new List<GameObject>();
            StartCoroutine(Spawner());
        }
        private IEnumerator Spawner()
        {
            yield return new WaitForSeconds(0);
            while (GameManager.Instance.state == GameManager.GameState.Game)
            {
                //Each spawn increases rate by 1%. 
                spawnRate -= spawnRate / 100;

                //getting asteroid
                GameObject asteroid = GetAsteroid();
                //calculating x-axis position
                float xPosition = Random.Range(-gameManager.raodSystem.roadWidth, gameManager.raodSystem.roadWidth);
                //setting position
                asteroid.transform.position = new Vector3(xPosition, 1, spawnDistance);
                yield return new WaitForSeconds(spawnRate);
            }

        }

    }
}



