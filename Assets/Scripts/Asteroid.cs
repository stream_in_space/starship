﻿using UnityEngine;

namespace Starship
{
    public class Asteroid : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed;
        private bool passed;

        private void OnEnable()
        {
            passed = false;
        }

        void Update()
        {
            // rotatate asteroid Y axix
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);

            //Gain bonus score
            if (transform.position.z < GameManager.Instance.starship.transform.position.z && !passed)
            {
                passed = true;
                GameManager.Instance.scoreSystem.ApplyBonusScore();
            }

            //return asteroid to their pool 
            if (transform.position.z < GameManager.Instance.starship.transform.position.z - 10)
            {
                GameManager.Instance.spawner.availableAsteroids.Add(this.gameObject);
                gameObject.SetActive(false);
            }
        }
    }
}
