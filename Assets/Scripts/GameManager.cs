﻿using UnityEngine;

namespace Starship
{

    public class GameManager : MonoBehaviour
    {
        public enum GameState
        {
            PreGame, Game, PostGame
        }
        public StarshipController starship;
        [HideInInspector] public CameraEffects cameraEffects;
        [HideInInspector] public RoadSystem raodSystem;
        [HideInInspector] public AsteroidsSpawner spawner;
        [HideInInspector] public ScoreSystem scoreSystem;
        [HideInInspector] public GameState state;

        private static GameManager _instance;
        public static GameManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
            state = GameState.PreGame;
        }
        private void Start()
        {
            raodSystem = GetComponent<RoadSystem>();
            cameraEffects = GetComponent<CameraEffects>();
            spawner = GetComponent<AsteroidsSpawner>();
            scoreSystem = GetComponent<ScoreSystem>();
        }
        public void GameOver()
        {
            raodSystem.enabled = false;
            spawner.enabled = false;
            scoreSystem.SaveHighestScore();
            scoreSystem.enabled = false;
            starship.enabled = false;
            state = GameState.PostGame;
        }
        private void Update()
        {
            if (state == GameState.PreGame)
            {
                if (Input.anyKeyDown)
                {
                    state = GameState.Game;
                    spawner.enabled = true;
                    starship.enabled = true;
                    scoreSystem.enabled = true;
                }
            }
        }

    }

}

