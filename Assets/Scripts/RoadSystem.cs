﻿using UnityEngine;

namespace Starship
{

    public class RoadSystem : MonoBehaviour
    {
        [SerializeField] private GameObject roadPartPrefab;
        [SerializeField] private int roadLength;
        public float roadWidth;
        [HideInInspector] public GameObject road;
        private GameObject roadPartsContainer;
        private int currentPosition;
        private int nextPointToReplaceRoad;
        private int roadSizeZ;
        private StarshipController starship;
        void Start()
        {
            roadSizeZ = (int)roadPartPrefab.GetComponent<MeshRenderer>().bounds.size.z;
            InitializeRoad();
            currentPosition = (int)transform.position.z;
            nextPointToReplaceRoad = currentPosition - roadSizeZ;
            starship = GameManager.Instance.starship;
        }
        private void InitializeRoad()
        {
            road = new GameObject("Road");
            roadPartsContainer = new GameObject("RoadParts");
            roadPartsContainer.transform.SetParent(road.transform);

            for (int i = 0; i < roadLength; i++)
            {
                Vector3 position = Vector3.forward * i * roadSizeZ;
                GameObject.Instantiate(roadPartPrefab, position, Quaternion.identity, roadPartsContainer.transform);
            }
        }

        private void Update()
        {
            //move the road
            road.transform.Translate(Vector3.back * starship.currentSpeed * Time.deltaTime);

            //checking if it's time to repalce a part of the road
            if (road.transform.position.z <= nextPointToReplaceRoad)
            {
                //updating position info
                currentPosition -= roadSizeZ;
                //getting next point to repalce a road;
                nextPointToReplaceRoad = currentPosition - roadSizeZ;

                //getting z position which will be used to place a new part of the road
                int newZPosition = ((roadLength - 1) * roadSizeZ) + (int)roadPartsContainer.transform.GetChild(0).transform.localPosition.z;
                Vector3 newPosition = Vector3.forward * newZPosition;
                roadPartsContainer.transform.GetChild(0).transform.localPosition = newPosition;
                roadPartsContainer.transform.GetChild(0).SetAsLastSibling();
            }
        }
    }
}

