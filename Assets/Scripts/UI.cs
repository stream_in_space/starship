﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Starship
{
    public class UI : MonoBehaviour
    {
        private void OnGUI()
        {
            var style = GUI.skin.GetStyle("Label");


            if (GameManager.Instance.state == GameManager.GameState.PreGame)
            {
                style.alignment = TextAnchor.UpperCenter;
                style.fontSize = 20;
                GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 200, 400, 50), "Press any key to start playing");
            }
            else if (GameManager.Instance.state == GameManager.GameState.Game)
            {
                style.alignment = TextAnchor.UpperLeft;
                style.fontSize = 20;
                GUI.Label(new Rect(10, 10, 1000, 50),
                 "Score : " + ((int)GameManager.Instance.scoreSystem.score).ToString() +
                 "   Asteroids passed : " + (GameManager.Instance.scoreSystem.asteroidPassed).ToString() +
                 "   Time played : " + ((int)GameManager.Instance.scoreSystem.timePlayed).ToString() +

                  "   Highest Score : " + (GameManager.Instance.scoreSystem.highestScore).ToString()
                );

            }
            else if (GameManager.Instance.state == GameManager.GameState.PostGame)
            {

                style.alignment = TextAnchor.UpperLeft;
                style.fontSize = 20;
                GUI.Label(new Rect(10, 10, 1000, 50),
                 "Score : " + ((int)GameManager.Instance.scoreSystem.score).ToString() +
                 "   Asteroids passed : " + (GameManager.Instance.scoreSystem.asteroidPassed).ToString() +
                 "   Time played : " + ((int)GameManager.Instance.scoreSystem.timePlayed).ToString() +

                  "   Highest Score : " + (GameManager.Instance.scoreSystem.highestScore).ToString()
                );

                style.alignment = TextAnchor.UpperCenter;
                style.fontSize = 50;
                GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 200, 400, 50), "GAME OVER");
                style.fontSize = 20;
                if (GameManager.Instance.scoreSystem.highestScoreIsReached)
                {
                    GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 400, 50), "Congratulations, you broke your high score!");
                }

                if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 + 100, 100, 50), "Restart"))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }

            }
        }

    }
}


