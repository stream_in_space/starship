﻿using UnityEngine;

namespace Starship
{
    public class ScoreSystem : MonoBehaviour
    {
        [HideInInspector] public float score;
        [HideInInspector] public int asteroidPassed;
        [HideInInspector] public float timePlayed;
        [HideInInspector] public int highestScore;
        [HideInInspector] public bool highestScoreIsReached;
        private int scoreMultiplier = 1;
        private int scoreBonus = 5;
        void Start()
        {
            highestScore = PlayerPrefs.GetInt("highestScore");
        }
        void Update()
        {
            score += Time.deltaTime * scoreMultiplier;
            timePlayed += Time.deltaTime;
            if (score > highestScore)
            {
                highestScore = (int)score;
            }
        }
        public void ApplySpeedMultiplier(bool apply)
        {
            scoreMultiplier = apply ? 2 : 1;
        }
        public void ApplyBonusScore()
        {
            asteroidPassed++;
            score += scoreBonus;
        }
        public void SaveHighestScore()
        {
            if (PlayerPrefs.GetInt("highestScore") < score)
            {
                PlayerPrefs.SetInt("highestScore", (int)score);
                PlayerPrefs.Save();
                highestScoreIsReached = true;
            }
        }

    }
}

