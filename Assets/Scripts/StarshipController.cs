﻿using UnityEngine;

namespace Starship
{
    public class StarshipController : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed = 6;
        [SerializeField] private float rotationSmooth = 5.0f;
        [SerializeField] private float accelerationSmooth = 5.0f;
        [SerializeField] private float tiltAngle = -60.0f;
        [SerializeField] private float targetSpeed = 15;
        [SerializeField] private float speedMultiplier = 2;
         public float currentSpeed;


        //detect obstacle
        private void OnTriggerEnter(Collider other)
        {
            GameManager.Instance.GameOver();
        }
        private void Update()
        {
            // getting input value
            var horizontalInputAxis = Input.GetAxis("Horizontal");


            // change ships z axis angle
            float tiltAroundZ = horizontalInputAxis * tiltAngle;
            Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * rotationSmooth);


            // change ships x axis position
            float xPosition = transform.position.x + (horizontalInputAxis * rotationSpeed * Time.deltaTime);
            xPosition = Mathf.Clamp(xPosition, -GameManager.Instance.raodSystem.roadWidth, GameManager.Instance.raodSystem.roadWidth);
            transform.position = new Vector3(xPosition, 1, 0);


            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.scoreSystem.ApplySpeedMultiplier(true);
            }

            // SpeedBosting. Road system uses currentSpeed value
            if (Input.GetKey(KeyCode.Space))
            {
                currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed * speedMultiplier, accelerationSmooth * Time.deltaTime);
                GameManager.Instance.cameraEffects.OnSpeedBoost();
            }
            else
            {
                currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed, accelerationSmooth * Time.deltaTime);
                GameManager.Instance.cameraEffects.OnNormalSpeed();
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                GameManager.Instance.scoreSystem.ApplySpeedMultiplier(false);
            }
        }
    }
}



